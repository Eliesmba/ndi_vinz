const PORT = 3000;
const DB_HOST = "localhost"
const DB_NAME = "ndi"
const DB_USER = "root"
const DB_PASS = ""

// ADMIN : admin/aa

var express = require("express");
var session = require('express-session')
var bodyParser = require("body-parser");
var crypto = require('crypto');
var cookieParser = require('cookie-parser');
var mysql = require('mysql');

var auth = require("./auth.js") // CHANGER FICHIER MODULE 

var db = mysql.createConnection({
  host: DB_HOST,
  user: DB_USER,
  password: DB_PASS,
  database: DB_NAME,
  multipleStatements: true
});

var app = express();

app.use(bodyParser());
app.use(cookieParser());
app.use(session({secret: "Shh, its a secret!", resave: true, saveUninitialized: true}));

db.connect(function(err) {
  if (err) throw err;
  console.log("Connecté à la base de données '"+ DB_NAME +"'");

  app.post("/login/auth", (req, res) => {
  	(async function() {
  		let NikitaBellucci = (await auth.login(req, db, crypto))[0];
  		res.send(NikitaBellucci);
  	})();
  	
  });

});
app.listen(PORT);
console.log("Server running : http://localhost:"+PORT+"/");
